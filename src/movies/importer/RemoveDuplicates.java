package movies.importer;
import java.util.*;

public class RemoveDuplicates extends Processor
{
	public RemoveDuplicates(String src, String dest)
	{
		super(src, dest, false);
	}

	public ArrayList<String> process(ArrayList<String> input) 
	{
		ArrayList<String> noDupes = new ArrayList<String>();
		for(String s : input)
		{
			noDupes.add(s);
		}
		
		for(int i = 0; i < noDupes.size(); i++)
		{
			int occurence = 0;
			for(String str : noDupes)
			{
				if(noDupes.get(i).equals(str))
				{
					occurence++;
				}
			}
			
			if(occurence >= 2)
			{
				noDupes.remove(i);
			}
		}
		
		return noDupes;
	}
}
