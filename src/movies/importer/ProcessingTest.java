package movies.importer;
import java.io.*;

public class ProcessingTest 
{
	public static void main(String[] args) throws IOException
	{
		//LowercaseProcessor Processor
		String src1 = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Lab5\\source";
		String dest1 = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Lab5\\destination";
		
		LowercaseProcessor proc1 = new LowercaseProcessor(src1, dest1);
		proc1.execute();
		
		//RemoveDuplicates Processor
		String src2 = dest1;
		String dest2 = dest1;
		
		RemoveDuplicates proc2 = new RemoveDuplicates(src2, dest2);
		proc2.execute();
	}
}
