package movies.importer;
import java.util.*;

public class LowercaseProcessor extends Processor 
{
	public LowercaseProcessor(String src, String dest)
	{
		super(src, dest, true);
	}
	
	public ArrayList<String> process(ArrayList<String> input)
	{
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(String str : input)
		{
			asLower.add(str.toLowerCase());
		}
	
		return asLower;
	}
}
